package com.example.tp1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        final RecyclerView countryList = (RecyclerView) findViewById(R.id.recycler_view);
        MyAdapter adapter = new MyAdapter(this, CountryList.getNameArray());
        countryList.setLayoutManager(new LinearLayoutManager(this));
        countryList.setAdapter(adapter);




    };
}


