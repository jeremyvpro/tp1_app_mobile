package com.example.tp1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class CountryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.country_activity);
        Intent intent = getIntent();

        String titre = intent.getStringExtra("title");
        Country country = CountryList.getCountry(titre);
        int identifier = getResources().getIdentifier(country.getmImgFile(), "drawable", getPackageName());
        ((ImageView) findViewById(R.id.image)).setImageResource(identifier);
        ((ImageView) findViewById(R.id.image)).setTag(country.getmImgFile());
        ((TextView) findViewById(R.id.title)).setText(titre);
        ((EditText) findViewById(R.id.capital)).setText(country.getmCapital());
        ((EditText) findViewById(R.id.langues)).setText(country.getmLanguage());
        ((EditText) findViewById(R.id.monnaie)).setText(country.getmCurrency());
        ((EditText) findViewById(R.id.population)).setText(Integer.toString(country.getmPopulation()));
        ((EditText) findViewById(R.id.superficie)).setText(Integer.toString(country.getmArea()));

        final Button button = findViewById(R.id.sauvegarder);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {

                String title = (String) ((TextView) findViewById(R.id.title)).getText();
                Country countryUpdate = CountryList.getCountry(title);

                String image = (String) ((ImageView) findViewById(R.id.image)).getTag();
                String capitale = ((EditText) findViewById(R.id.capital)).getText().toString();
                String langues = ((EditText) findViewById(R.id.langues)).getText().toString();
                String monnaie = ((EditText) findViewById(R.id.monnaie)).getText().toString();
                String population =  ((EditText) findViewById(R.id.population)).getText().toString();
                String superficie = ((EditText) findViewById(R.id.superficie)).getText().toString();

                countryUpdate.setmCapital(capitale);
                countryUpdate.setmLanguage(langues);
                countryUpdate.setmCurrency(monnaie);
                countryUpdate.setmPopulation(Integer.parseInt(population));
                countryUpdate.setmArea(Integer.parseInt(superficie));
                countryUpdate.setmImgFile(image);

                finish();
            }

        });



    }

}