package com.example.tp1;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {


    private String[] item ;
    private Context mContext;

    public MyAdapter(Context context, String[] countryList ) {
        item = countryList;
        mContext = context;
    }
    @Override
    public int getItemCount(){
        return item.length;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType){
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.recycler_view_layout, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.pays.setText(item[position]);
        int identifier = mContext.getResources().getIdentifier(CountryList.getCountry(item[position]).getmImgFile(), "drawable", mContext.getPackageName());
        holder.image.setImageResource(identifier);

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private final TextView pays;
        private final ImageView image;


        public MyViewHolder(final View itemView) {
            super(itemView);

            pays = ((TextView) itemView.findViewById(R.id.pays));
            image = ((ImageView) itemView.findViewById(R.id.image_recycler));

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(mContext, CountryActivity.class);
                    intent.putExtra("title", pays.getText());
                    mContext.startActivity(intent);
                }
            });
        }


    }




}
